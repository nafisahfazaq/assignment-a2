//to give pop up notif
// alert('Hello there!');

// to give on console
console.log('Hello there!');
console.error('This is an error');
console.warn('This is a warning');

//let, const
let age = 22;
const name = 'fuwa';

console.log(name);
console.log(age);

// string, number, boolean, null, undefined
const name2 = 'alfa';
const age2 = 25;
const rating = 8.1;
const isCool = true;
const n = null;
const u = undefined;
let z;

//typeof to check the type of var
console.log(typeof z)

// concatenation = rangkaian
console.log('My name is' + name + 'and I am' + age);
// template string
const hi = `My name is ${name} and I am ${age}`;

console.log(hi);

// to know the length of a string
console.log(hi.length);
// to set upper and lower case
console.log(name2.toUpperCase());
console.log(hi.toLowerCase());
// to cut the string
console.log(hi.substring(7, 21).toUpperCase);
// to split string to char
console.log(name.split(''));
/*multi
comment*/
// arrays in js can contains many type of var
const animal = ['lion', 'eagle', 'fox', 21, false];

// add values to arrays
animal[5] = 'rat'; //manual
animal.push('cat'); //if the length are big
animal.unshift('birb'); //put it in the first
animal.pop(); //take out the last
console.log(Array.isArray(animal)); //to make sure the type
console.log(animal.indexOf(21)); //to find the index of a value
console.log(animal);

// object in js
const person = [
    {
        firstName: 'Fuwa',
        lastName: 'Tama',
        age: 22,
        hobbies: ['drawing', 'gaming', 'reading'],
        address: {
            street: 'in',
            city: 'your',
            state: 'heart'
        }
    },
    {
        firstName: 'Alfa',
        lastName: 'Rio',
        age: 25,
        hobbies: ['studying', 'gaming', 'reading'],
        address: {
            street: 'in',
            city: 'your',
            state: 'head'
        }
    },
    {
        firstName: 'Rodhie',
        lastName: 'Rich',
        age: 25,
        hobbies: ['racing', 'watch a movie', 'shopping'],
        address: {
            street: 'in',
            city: 'my',
            state: 'face'
        }
    }
];

person.email = 'fuwafuwa@gmail.com'; //to add
console.log(person);
console.log(person[1].address[2]);

const {firstName, lastName, address} = person; //to take out the person's elements
console.log(lastName);

const personJSON = JSON.stringify(person); //to change array to JSON
console.log(personJSON);

//for
for(let i = 0; i < person.length; i++){
    console.log(person[i].firstName);
}

for(let persons of person){
    console.log(persons.firstName);
}

//while
let i = 0;
while(i < 10){
    console.log(`While Loop Number: ${i}`);
    i++;
}

//forEach, map, filter (high order function)
person.forEach(function(persons){
    console.log(persons.firstName);
}); //calls a function for each element in an array

const personFirstName = person.map(function(persons){
    return persons.firstName;
}); //creates new array from calling a function for every array element

console.log(personFirstName);

const personAge = person.filter(function(persons){
    return persons.age === 22;
}); //filter for filtering

console.log(personAge);

const personAddress = person.filter(function(persons){
    return persons.address.city === 'my';
}).map(function(persons){
    return persons.age;
})

console.log(personAddress);

//if
const a = 32
const b = 25

if(a > b){
    console.log('a more than b');
} else console.log('b more than a');

if(a > 20 || b < 10){
    console.log('true');
}

if(a > 20 && b < 10){
    console.log('true');
} else console.log('false');

//switch case
const fruit = a < 20 ? 'strawberry' : 'banana'; //? = then

switch(fruit){
    case 'strawberry':
        console.log('wow it is a strawberry!');
        break;
    case 'banana':
        console.log('wow it is a banana! BANANA!!!');
        break;
    default:
        console.log('no fruit available');
        break;
}

//function
function addNumb(num1, num2){
    return num1 % num2;
}

console.log(addNumb(17, 3));

//alternative function (arrow function)
const addNumbs = (num3 = 5, num4 = 3) => console.log(num3 % num4);

//es6 class
//constructor function (PascalCase means object)
function Person(firstName, lastName, dob){
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = new Date(dob);
    this.getBirthYear = function() {
        return this.dob.getFullYear();
    }
}

Person.prototype.getFullName = function() {
    return `${this.firstName} ${this.lastName}`;
}

//class
class Personn {
    constructor(firstName, lastName, dob){
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = new Date(dob);
    }
    getBirthYear() {
         return this.dob.getFullYear();
    }

    getFullName() {
        return `${this.firstName} ${this.lastName}`;
    }
}

//instantiate object
const person1 = new Person('Alfa', 'Araska', '3-3-1998');
const person2 = new Person('Fuwa', 'Tama', '17-1-2000');

console.log(person1.getBirthYear());
console.log(person1.getFullName());

//next to css

//get single element from doc 1.16.05
console.log(document.getElementById('my-form'));
console.log(document.querySelector('h1'));

//get multiple element from doc
console.log(document.querySelectorAll('.item'));
console.log(document.getElementsByClassName('item'));
console.log(document.getElementsByTagName('h1'));

//unordered list
const ul = document.querySelector('.items');

// ul. remove();
// ul.lastElementChild. remove();
ul.firstElementChild.textContent = 'Hello';
ul.children[1].innerText= 'Brad';
ul.lastElementchild.innerHTML ='<h1>Hello</h1>';

const btn = document.querySelector('.btn');
btn.style.background = 'red';

//addEventListener
//doc query selector
btn.addEventListener('mouseout', (e) => {
    e.preventDefault();
    document.querySelector('#my-form'). style.background = '#ccc';
    document.querySelector('body').classList.add('bg-dark');
    document.querySelector('.items')
    .lastElementChild.innerHTML = '<h1>Helloc/h1>';
});

//
const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name') ;
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

myForm.addEventListener('submit', onSubmit);

function onSubmit(e) {
    e.preventDefault();

    if(nameInput.value === '' || emailInput.value === '') {
        msg.classList.add('error');
        msg.innerHTML = 'Please enter all fields';
        
        setTimeout (() => msg.remove(), 3000);
    }else {
        const li = document.createElement('li');
        li.appendChild(document.createTextNode('${nameInput.value} : ${femailInput.value}'));
        
        userList.appendChild(li);

        // Clear fields
        nameInput.value = '';
        emailInput.value = '';

        // console. log('success');
    }
}