package main //name of the code

import {
	"fmt" //import go package to use print etc
	"strings"
}

var conferenceName = "Fuwa Conference"
const conferenceTickets = 50

//func = begin in pascal
func main() {

	greetUser(conferenceName, ticketsSold, remainingTickets)
	
	fmt.Printf("array length %v", len(bookings))

	//ask user data
	for remainingTickets > 0 && len(bookings) <= 50 {
		
		userName, userTickets, userEmail := getUserData()

		isValidName, isValidEmail, isValidTicketsNumber := validateUserInput(userName, email, userTickets, remainingTickets)

		if isValidName && isValidEmail && isValidTicketsNumber {
			if userTickets <= remainingTickets (
				
				bookTicket(userName, userEmail, userTickets, bookings)

				if remainingTickets == 0 {
					//end program
					fmt.Println("Our tickets has sold out!\n Thank you for your enthusiasm!\n Please come back next year!")
					break
				}
			} else {
				fmt.Printf("We only have %v tickets remaining, so you can't book %v tickets\n", remainingTickets, userTickets)
				continue //to skip the wrong loop
			}
		} else {
			if !isValidName {
				fmt.Println("Your entered name is too short. Please retry.")
			}
			if !isValidEmail {
				fmt.Println("Your entered email doesn't contain '@'. Please retry.")
			}
			if !isValidTicketsNumber {
				fmt.Println("Your tickets amount are invalid. Please retry.")
			}
		}
	} 
	//ctrl + c in terminal to stop the looping
	//in go there're no while do while etc, just for for looping

	/*switch case in go can contain >1 case and doesn't need break
		city := "London"

		switch city {
			case "New York"
				// execute code
			case "Singapore", "Malaysia"
				//execute code
			case "London"
				//execute code
		}*/
}

func greetUser(confName string, ticketsSold int, remainingTickets int) {

	//welcome page
	fmt.Printf("Welkie Dokie to %v booking apps!", confName) //%v for referencing to var. To use it use printf f, f for format
	//%v can comes in many ways, depends on what do you want to get (it can be types of data, etc)
	fmt.Printf("%v tickets has been sold! We only have %v remaining tickets now!", ticketsSold, remainingTickets)
	fmt.Println("Go grab your ticket fast now!")

	fmt.Println(&remainingTickets) //use & to see the memory location
}

func printUserName(bookings []string) {
	userNames := []string{}
	for _, booking := range bookings{ //_ uses for blank identifirer, so unused var can be put in there (it should be "index" instead of _, but index were unused)
		var names = string.Fields(booking) //strings.Fields() splits the string with with space as separator
		var userName = names[0]
		userName = append(userName, userNames)
	}
	
	fmt.Println("These are who has booked our tickets %v\n", userName)
}

func validateUserInput(userName string, email string, userTickets int, remainingTickets int) (bool, bool, bool) {
	isValidName := len(userName) >= 2
	isValidEmail := strings.Contains(email, "@")
	isValidTicketsNumber := userTickets > 0 && userTickets <= remainingTickets

	//isInvalidCity := city != "Singapore" || city != "London"
	
	return isValidName, isValidEmail, isValidTicketsNumber
}
//type boolean outside the parameters used for the type of the return

func getUserData(string. int, string) {
	var userName string
	var userEmail string
	var userTickets int

	fmt.Println("Hello! Who is this?")
	fmt.Print("Hi! I'm ")
	fmt.Scan(&userName) //use & to assign user's input to memory
	
	fmt.Println("How many tickets you want to buy?")
	fmt.Scan(&userTickets)
	
	fmt.Println("Please enter your email to get the invoice.")
	fmt.Print("Okay, here is my email ")
	fmt.Scan(&userEmail)

	return userName, userTickets, userEmail
}

func bookTicket(userName string, userEmail string, userTickets int, bookings[]string) {
	var ticketsSold = userTickets
	var remainingTickets = conferenceTickets - ticketsSold
	var bookings = []string{} //slices = dynamic array (no initial length)

	//totalPurchase := userTickets * 100000 //auto create var and assign it's value

	fmt.Printf("Hi, %v! Thank you for purchasing %v ticket(s). Please kindly check your email for invoice.\n", userName, userTickets)

	//to assign some var to one index in array
	bookings = appends(bookings, userName + "-" + userEmail + "-", userTickets) //use append to add value

	printUserName(bookings)

	// noTicketsRemaining := remainingTickets == 0

	return 
}

/* Notes:
- to make the code simpler, take the func to another docs. set the package to main.
- when run, run all of the packages

- multiple packages work wih folder. set the packages to folder's name
- to call other packages to main, put it to import. for multiple package, just input the folder's name
- var in other packages need to be exported to be able to be used in another package
  ^change the func name to PascalCase

 Scope of variables
	- local : declared within function or block. Can be used only within that function or that block
	- package : declared outside all functions. Can be used everywhere in the same package
	- global : declared outside all function & uppercase first letter. Can be used everywhere across all packages

create a map :
var userData = make(map[string]string) << all keys and values have the same data type
userData["userName"] = userName
userData["userEmail"] = userEmail
userData["userTickets"] = userTickets
strconv.Format(Uint64(userTickets), 10) << to convert string value to another value stated

Struct : a way to group several related variables into one place. Each variable in the structure is known as a member of the structure.
type UserData struct{      // Structure declaration
  userName string     // Member (int variable)
  userEmail string
  userTickets int
}

goroutins = lighter threading in go, more like multi threading
package sync = basic synchrinization functionalit
		ads  = sets the number of goroutines to wait for
		wait = blocks until the WaitGroup counter = 0
		Done = decrements the WaitGroup counter by 1
*/